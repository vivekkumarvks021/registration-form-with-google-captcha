import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
  FormArray,
} from "@angular/forms";

@Component({
  selector: "app-reavtive-reg-form",
  templateUrl: "./reavtive-reg-form.component.html",
  styleUrls: ["./reavtive-reg-form.component.css"],
})
export class ReavtiveRegFormComponent implements OnInit {
  registrationForm: FormGroup;
  address: FormArray;

  occupationValue = [{
    name:"Software Developer",
    value:"SD"
  },{
    name:"Software Engineer",
    value:"SE"
  }]

  regex = {
    number: new RegExp("^(0|[1-9][0-9]*)$"),
    email: new RegExp(
      "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$"
    ),
    mobile: /^([6-9][0-9]*)$/,
    pincode: new RegExp("^[1-9]{1}[0-9]{2}\s{0,1}[0-9]{3}$")
  };

  constructor(private _fb: FormBuilder) {}

  createForm() {
    this.registrationForm = this._fb.group({
      name: new FormControl("", Validators.required),
      age: new FormControl("", [
        Validators.required,
        Validators.pattern(this.regex.number),
      ]),
      email: new FormControl("", [
        Validators.required,
        Validators.pattern(this.regex.email),
      ]),
      mobile: new FormControl("", [
        Validators.required,
        Validators.pattern(this.regex.mobile),
        Validators.minLength(10)
      ]),
      occupation: new FormControl("", Validators.required),
      address: this._fb.array([this.createAddress()]),
      recaptcha: new FormControl("", Validators.required),
      agree:""
    });
  }

  createAddress(): FormGroup {
    return this._fb.group({
      address1: new FormControl("", Validators.required),
      address2: new FormControl("", Validators.required),
      city: new FormControl("", Validators.required),
      state: new FormControl("", Validators.required),
      pincode: new FormControl("", [Validators.required,Validators.pattern(this.regex.pincode)]),
    });
  }

  addAddress(): void {
    //this.registrationForm.get('address').push(this.createAddress());
    this.address = this.registrationForm.get("address") as FormArray;
    this.address.push(this.createAddress());
  }

  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: ${captchaResponse}`);
  }

  onSubmit() {
    console.log("Sumitted");
  }

  checkFormData(){
    console.log(this.registrationForm);

  }

  ngOnInit(): void {
    this.createForm();
  }
}
