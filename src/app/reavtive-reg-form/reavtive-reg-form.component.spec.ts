import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReavtiveRegFormComponent } from './reavtive-reg-form.component';

describe('ReavtiveRegFormComponent', () => {
  let component: ReavtiveRegFormComponent;
  let fixture: ComponentFixture<ReavtiveRegFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReavtiveRegFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReavtiveRegFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
